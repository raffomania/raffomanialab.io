/**
 * @type [RegExp, string][]
 */
const rules = [[/ss$/, 'sses']];

/**
 *
 * @param {string} singular
 * @param {number} count
 * @returns string
 */
export function pluralize(singular, count) {
	if (count < 2) {
		return singular;
	}

	for (let [pat, replacement] of rules) {
		if (singular.match(pat) !== null) {
			return singular.replace(pat, replacement);
		}
	}
	return `${singular}s`;
}
