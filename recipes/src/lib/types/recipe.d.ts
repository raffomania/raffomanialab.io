export interface Recipe {
	title: string;
	steps: Step[];
	authoredServings: number;
	servingLabel?: string;
	image?: {
		url: string;
		credits?: {
			url: string;
			description: string;
		};
	};
}

export type Step = StepPart[];

export type StepPart = string | Ingredient | IngredientGroup;

export interface Ingredient {
	n?: number;
	ingredient: string;
	unit?: string;
}

export type IngredientGroup = Ingredient[];

export function isIngredient(stepPart: unknown): stepPart is Ingredient;
export function isIngredientGroup(stepPart: unknown): stepPart is IngredientGroup;
