export function isIngredient(stepPart) {
	return typeof stepPart === 'object' && stepPart !== null && 'ingredient' in stepPart;
}

export function isIngredientGroup(stepPart) {
	return Array.isArray(stepPart) && stepPart.every(isIngredient);
}
