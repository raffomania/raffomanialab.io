import { base } from '$app/paths';
import { browser } from '$app/environment';
import { preloadData } from '$app/navigation';

/** @type {import('./$types').PageLoad} */
export async function load() {
	/** @type Record<string, { recipe: import('$lib/types/recipe').Recipe }> */
	const recipeFiles = import.meta.glob('./r/*/+page.svelte', { eager: true });
	const recipeRoutes = Object.entries(recipeFiles).map(([path, content]) => {
		const cleaned = path.replace('+page.svelte', '').replace('./', '');
		const finalPath = `${base}/${cleaned}`;
		if (browser) {
			preloadData(finalPath);
		}
		return {
			path: finalPath,
			recipe: content.recipe,
		};
	});

	return { recipeRoutes };
}
