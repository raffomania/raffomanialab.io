import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter(),
		prerender: {
			handleHttpError: ({ status, path, referrer, referenceType }) => {
				if (!path.startsWith('/recipes')) {
					return;
				}

				throw new Error(
					`${status} ${path} (from ${referrer}, ${referenceType}): failed to build a page`
				);
			},
		},
		inlineStyleThreshold: 4096,
		paths: {
			base: '/recipes',
		},
	},
};

export default config;
