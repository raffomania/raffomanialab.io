# Intelligente Assistenten


## Inhalt
- Worum geht es?
- Sprachverarbeitung
- Der Turing-Test
- Die Zukunft


## Was sind Intelligente Assistenten?
//
- Einmalige, spezielle Aufgaben
- Zugriff auf Sensoren (z.B. Location, Temperatur)
- Zugriff auf externe Informationen (Wetter, Kalender)


### Clippy
![Clippy](img/clippy.jpg)

<small class="footnote">http://joke-battles.wikia.com/wiki/Clippy</small>
//
- Schablonen basiert
- Eher nicht intelligent (keine sensorik, keine externen infos)


<!-- .slide: class="only-image" -->
### Siri / Google Assistant / Cortana
![Siri](img/siri.jpg)

<small class="footnote">http://bgr.com/2016/06/15/apples-digital-helper-siri-needs-help</small>


<!-- .slide: class="only-image" -->
### Alexa
![Alexa](img/alexa.jpg)

<small class="footnote">http://www.giga.de/audio/amazon-echo/tipps/wann-erscheint-amazon-echo-in-deutschland/</small>


![Google Assistant](img/google_assistant.jpg)

<small class="footnote">http://www.computerworld.com/article/3072234/artificial-intelligence/google-gets-smart-with-google-assistant-and-google-home.html</small>
//
- Gemeinsamkeit: Sprachein- und Ausgabe


## Computer und Sprache


<iframe width="960" height="600" src="https://www.youtube.com/embed/0rAyrmm7vv0?showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
//
- 1939 Voder Speech Synthesizer
- 1952 bell labs erste simple spracherkennung (ziffern)


## Aktuelle Verfahren
//
- Fortgeschritten: Man kann quasi diktieren
- Machine Learning
- Datensammeln
- Schritt in Richtung von Transparent Computing


## Der Turing-Test
//
- Weg vom Interface, hin zur "Intelligenz"


<!-- .slide: class="only-image" -->
![Der Turing-Test](img/turing_test.png)


## ELIZA
//
- Kein persönlicher Assistent
- Nachfragen, Wiederholen


<!-- .slide: style="text-align: left;" -->
Men are all alike.

*In what way*

They're always bugging us about something or other.

*Can you think of a specic example*

Well, my boyfriend made me come here.

*Your boyfriend made you come here*
//
Hier zu sehen: subtile fragen nach mehr


<!-- .slide: style="text-align: left;" -->
He says I'm depressed much of the time.

*I am sorry to hear you are depressed*

It's true. I am unhappy.

*Do you think coming here will help you not to be unhappy*
//
- Das gleiche: einfache Schablonen
- Wirkt durch Bezug auf den Partner


## Der Loebner-Wettbewerb
//
- PC Therapist: wie ELIZA


<!-- .slide: style="text-align: left;" -->
*Welcome judge 6*

hello

*Hello? Hello?*

It's going to waste a lot of time if you repeat everything.

*Rock out!*

Rock on! Don't you know anything?

*Cool. Do you like Nine Inch Nails? I don't want to discuss religion today.*
//
- Functional Response Emulation Device
- FRED: Lernen aus vorherigen Antworten
- non sequitur


<!-- .slide: style="text-align: left;" -->
you're a cleaner?

*Yeah? You think I'm a thief, too? You're no better than the rest of them.
Leave me alone, asshole.*

asshole?

*I don't know.*

you called me an asshole?

*Huh?*
//
- TIPS: eine Story, agressiv
- *Huh?*: kein echtes verständnis, oft blanko antwort
- Gewinner verwenden nur tricks


## Heute
//
- Loebner-Wettbewerb ist nicht relevant für die Anwendung
- Watson zeigt: computer können Spracherkennung, sind aber nicht wirklich "intelligent"
- Einfach nur helfen, keine Emotionen
- Cloud offloading, machine learning


## Fragen?


## Diskussion


- Brauchen wir emotionale Intelligenz in Software?
- Welche moralischen Implikationen hätte echte emotionale Intelligenz in Software?


- Könnte man ein Programm wie ELIZA o.Ä. als tatsächlichen Therapeuten einsetzen?
//
in den 60ern haben sich tatsächliche Psychologen dafür ausgesprochen!


## Referenzen
- W. Rapaport: The Turing Test, in Keith Brown (ed.), Encyclopedia of Language and Linguistics, 2nd Edition, Vol. 13, pp. 151-159, Oxford: Elsevier, 2006.
- J. Hutchens. 1997. How to Pass the Turing Test by Cheating. Technical Report. University of Western Australia.
- A. Nusca: Say command: How speech recognition will change the world, Smart Takes, SmartPlanet, 2011
- IBM's Watson Supercomputer Destroys Humans in Jeopardy, 2011 https://www.youtube.com/watch?v=WFR3lOm_xhE
