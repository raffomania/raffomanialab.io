![love2d logo](img/keepcalm.png)


# Lua
## A simple scripting language


## No Classes, no Objects
- Just a bunch of instructions
- Variables and functions are globally accessible


## No semicolons

```lua
print("hi!")
doSomething()
print("done!")
```


## Dynamically typed

```lua
name = "Doge"
age = 20

print(name .. " is " .. age .. " years old.")
```


## Functions

```lua
function shout(message)
    print(message .. "!1!!")
end
```
Note: - No curly braces!


## if then else

```lua
name = "Doge"
if name == "Doge" then
    print("Everything OK")
elseif name == "Rick Astley" then
    print("Surprise Rick Roll!")
else
    print("There is something wrong here")
end
```


## Lists

```lua
food = {"Spaghetti", "Pizza", "Coke"}
food[1] == "Spaghetti"
```
Note: - 1-indexed!


# Löve 2d
Get ready to rumble!


## Hi, World

```lua
function love.draw()
    love.graphics.print("Hi, World!", 200, 100)
end
```


## Game Loop

```lua
function love.load()
end

function love.update()
end

function love.draw()
end
```


## A better game
```lua
function love.load()
    image = love.graphics.newImage("doge.jpg")
    xPosition = 50
end

function love.update()
    xPosition = xPosition + 5
end

function love.draw()
    love.graphics.draw(image, xPosition, 100)
end
```


## Input

```lua
...

function love.update()
    if love.keyboard.isDown("right") then
        xPosition = xPosition + 5
    elseif love.keyboard.isDown("left") then
        xPosition = xPosition - 5
    end
end

...
```


## Get it running

(For detailed instructions, see the [Getting Started Guide](http://www.love2d.org/wiki/Getting_Started))

- [Install Löve](http://love2d.org)
- Create a new directory for your game
- Put your code into a file named main.lua
- Windows users: [Read the Getting Started Guide](http://www.love2d.org/wiki/Getting_Started)
- Linux users: execute
```bash
love ~/path/to/yourgame
```
- OS X users: execute
```bash
/Applications/love.app/Contents/MacOS/love ~/path/to/yourgame
```


# Keep Going!

- [Download Löve2d](http://love2d.org) and start coding!
- Look for advanced stuff in the [Löve2d Wiki](http://www.love2d.org/wiki/Main_Page)
- For questions about Lua, see [this awesome tutorial](http://www.lua.org/pil/contents.html)
