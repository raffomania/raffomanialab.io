import { defineWindiSetup } from "@slidev/types";

export default defineWindiSetup(() => ({
    theme: {
        extend: {
            // fontSize: {
            //     xs: "1rem",
            //     sm: "1.125rem",
            //     tiny: "1.125rem",
            //     base: "1.25rem",
            //     lg: "1.5rem",
            //     xl: "1.875rem",
            //     "2xl": "2rem",
            //     "3xl": "2.25rem",
            //     "4xl": "3rem",
            //     "5xl": "4rem",
            //     "6xl": "5rem",
            //     "7xl": "6rem",
            // },
            colors: {
                yellow: "#fffb3d",
                purple: "#d01b91",
            },
        },
    },
}));
