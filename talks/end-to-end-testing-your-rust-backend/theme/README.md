# Slidev Defaux theme

A cusomized version of the default theme for [Slidev](https://github.com/slidevjs/slidev).

## Install

At the moment, you need to put this folder into the `theme` directory of your slidev project and set it as a local theme in your `slides.md` frontmatter.

## License

MIT License
