---
theme: ./theme
# https://sli.dev/custom/highlighters.html
background: /eurorust-background.jpg
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# persist drawings in exports and build
drawings:
  persist: false
# use UnoCSS
css: windicss
title: End-to-end Testing Your Rust Backend - Rafael Epplée
titleTemplate: '%s'
info: |
    Patterns for comprehensive and productive testing
routerMode: hash

layout: cover
---

<div class="my-32"></div>

<h1>
End-to-End Testing
<br>
Your Rust Backend
</h1>

Patterns for comprehensive and productive testing

Rafael Epplée

<p class="float-right text-right">
EuroRust
<br>
<span class="text-gray-500">2022-10-13</span>
</p>

<!-- 
Hello everyone, thanks for joining me today!
I'll be talking about end-to-end tests for your rust web backend and show some techniques that can help making your tests more productive.
 -->

---
layout: intro
---

<img src="/portrait.jpg" class="w-60 rounded-lg border-yellow border-4 float-right mr-12 ml-8" />

# Rafael Epplée

Full stack web developer
<br>
Working on maintainability, reliability, clean code, tooling

<div class="grid grid-cols-[2em,1fr] w-min justify-items-start items-center gap-y-3 my-10">
  <ri-link-m /> 
  <a href="https://www.rafa.ee">rafa.ee</a>
  <ri-github-line /> 
  <a href="https://github.com/raffomania">raffomania</a>
  <ri-twitter-line /> 
  <a href="https://twitter.com/rafaelepplee">@rafaelepplee</a>
</div>

<!--
- A short intro, I'm Rafael, a freelance full stack web developer from Hamburg here in Germany
- In my work I'm always looking for ways to create maintainable and well-working software
- I think Rust provides a very productive environment for writing maintainable software while keeping up development velocity over the long term
- Today, I want to share some techniques I learned during my work to make that happen
-->

---
layout: two-cols
---

# Testing Backends in Rust

- Maintainability, reliability and developer productivity
- A young discipline
- Can be hard

::right::

<div class="mt-20"></div>

<img src="/crabtesting.jpg" alt="" class="w-80 rounded-md mx-auto">

<!-- todo maybe add image here -->

<!-- 
- In recent years, I've mainly been doing web development using Rust, and that has involved writing lots of tests.
- Tests can provide a huge boost to maintainability, reliability and developer productivity.
- The rust ecosystem for web development is still quite young, and I've noticed that it can be hard to find resources on web testing for rust.
- Simple approaches to web testing can lead to a frustrating test experience.
- So today I want to present some patterns I've found helpful for testing web backends, specifically testing using end-to-end tests.
 -->

---

# End-to-End Testing Tradeoffs

In the context of REST APIs

<div grid="~ cols-2 gap-x-" class="mt-8">

<div v-click>

<h3 class="text-center">🚀</h3>

<hr>

- Like production
- Catch more bugs, e.g.
  - Errors in SQL queries
  - Routing configuration errors
  - Unexpected changes in serialization schemata
- Catch critical bugs

</div>

<div v-click>

<h3 class="text-center"><span class="inline-block transform rotate-90">🚀</span></h3>

<hr>

- Slow
- Obscure
- Unreliable
- Verbose code

</div>
</div>

<!-- 
- What are end-to-end tests and why do I like them?
- For this talk, I'm focusing on end-to-end testing REST APIs specifically
- You might call these tests integration tests as well
- Our definition is: Tests that assert the complete system's behavior from an outside perspective.
- For REST APIs, this means sending requests to the backend and inspecting the responses.
- *click* Advantages of this kind of testing are that the setup resembles a production environment pretty closely, and they can catch many more bugs than unit tests, for example
- Errors in SQL queries, routing config errors, unexpected changes in the interface to the outer world
- They also make it more natural to verify the system from the perspective of an actual user, meaning they catch more critical bugs
- *click* **Potential** drawbacks are that they can quickly become the slowest part of your test suite
- Because they involve a large part of the system, it can be hard to pinpoint where exactly a failure was caused
- Because of their complexity, non-determinism can sneak in, making them unreliable
- Their code can become large, cumbersome to write and hard to read
- Simplified, there are two issues here: test performance and ergonomics. I want to show how Rust enables us to tackle both
 -->

---
layout: two-cols
---

## Speed matters

- Feedback loop
- Code review cycles

<div class="mt-4">

## Ergonomics Matter

- Productivity
- Coverage
- Fun
- Correctness

</div>

::right::

<img src="/crabcycle.jpg" alt="" class="rounded-md">

<!--
- We saw that test speed and ergonomics can make end-to-end tests more useful
- here are some more reasons to care for fast and ergonomic tests
- And for any kind of test, a fast feedback loop during development improves productivity
- Fast CI runs shorten review cycle times
- With ergonomics, we make test authoring more productive, we have more time to write more *comprehensive* tests, it enables us to think up more cases to test
- Not to mention the fun you have, motivating you to write more tests
- Ergonomics also reduce bugs in tests by clarifying what is tested and how, making them easier to understand
-->

---
layout: section
---

# Agenda

<hr class="mb-8 mx-auto w-32">

<div class="text-left w-[fit-content] mx-auto">

## 1. Database Access

## 2. Concise Tests

## 3. Generic Tests

</div>

<!-- 
- Today I'd like to present three topics about making your tests faster and more ergonomic.
- Database access can quickly slow down tests and we'll see how to prevent that
- We'll apply the builder pattern to encapsulate common boilerplate code
- And we'll see how to to cover multiple similar features using a single generic test
 -->

---
layout: section
background: /crabservers.jpg
---

# 1. Database Access

Blazingly fast 🚀 

<!-- 
- Web backends often need a persistent database, which can quickly slow down end-to-end tests.
- Let's take a closer look at the problem.
 -->

---

# Popular Approaches

And their drawbacks

<br>
To prevent database changes from one test affecting another, we could:
<br>
<br>

| | | |
| --- | --- | --- |
| Run tests sequentially, reset database after each test | ➡ | Makes tests slower |
| Mock database | ➡ | Tests catch less bugs |
| Carefully run all tests in the same database | ➡ | Cumbersome, hard to get right |
| Use SQLite in-memory databases | ➡ | Only works with SQLite |


<style>
td:not(:last-child) {
  padding-right: 4em !important;
}
</style>

<!--
- Fundamentally, we want to ensure that changes in the database made in one test do not affect any other test.
- Here are some popular approaches to achieve this:
- Running tests sequentially is like a global Mutex on the database. Each test cleans up after itself, leaving a pristine database for the next one. This is slower than it could be.
- Mocking the database is fast, but makes the tests catch less database-related bugs.
- Running all tests in the same database means avoiding conflicts by inserting random data only, not modifying rows unrelated to the current test, and tolerating noise from other tests when fetching multiple rows.
- SQLite in-memory databases are a very elegant solution to our problem, but sometimes you just need Postgres.
- As you see, these approaches all have their drawbacks. Let's look at two other approaches that avoid these.
-->

---

# First Approach: Transactions

1. Wrap each test in a database transaction
1. Inject the per-test transaction into application
1. Roll back transaction after the test

<div grid="~ cols-2 gap-x-4" class="mt-8" v-click>

<div>

<h3 class="text-center">🚀</h3>

<hr>

- Closer to production environment than mocks
- Faster than sequential test execution
- Relatively simple to implement
- Commonly used and well-understood

</div>

<div>

<h3 class="text-center"><span class="inline-block transform rotate-90">🚀</span></h3>

<hr>

- Requires ACID-compliant database
- Needs a separate application instance for each test
- Database locks can cause contention
- Nested rollbacks are tricky to implement correctly

</div>
</div>
<!-- todo: other drawbacks? -->

<!--
- The first approach uses the guarantees provided by ACID-compliant databases.
- By wrapping each test in a single transaction that is never committed but rolled back, we can effectively guarantee that no connections but the one used by the test can see its changes.
- In rust, we can inject the transaction into request handlers via a shared state mechanism, or the common `FromRequest` trait, as provided by actix-web and axum for example.
- *click* With this approach we can have concurrent tests!
- It's using a real database, so it will hopefully catch more bugs than tests using a mock.
- It's relatively straightforward to implement and you will find articles about it online.
- However, it's only possible to do when your database provides certain guarantees around transactions.
- You will need to create an instance of your application for each test to inject the test-specific transaction.
- A little surprisingly for my team, you can run into problems with row- or table-level locks, making your tests slow again.
- Lastly, because the application is running in a transaction, rollbacks in your application code will have to use checkpoints to emulate nested transactions, which can be error-prone.
-->

---

# Second Technique: Cloning Databases

In PostgreSQL

1. Create a template database
```sql
CREATE DATABASE template_database;
-- Insert migrations here
```
2. For each test, clone the template, e.g.:

```sql
CREATE DATABASE cloned_database_1234567 TEMPLATE template_database;
```

3. Inject per-test database into application
3. Drop database after the test

<!-- 
- To avoid locks, we can use Postgres database templates.
- They allow us to effectively clone an existing database.
- So we create a template database, with migrations applied, and clone it for each test using this syntax.
- We tell our application to use this database and drop it after the test is done.
 -->

---

# Second Technique: Cloning Databases

In PostgreSQL

<div class="mt-16"></div>

<div grid="~ cols-2 gap-x-4" class="mt-8">

<div>

<h3 class="text-center">🚀</h3>

<hr>

- Faster than sequential test execution
- No locking problems
- No nested rollbacks

</div>

<div>

<h3 class="text-center"><span class="inline-block transform rotate-90">🚀</span></h3>

<hr>

- Usually less performant than transactions
- Disk write speed may become a bottleneck
- Needs a per-test application instance

</div>
</div>

<!--
- This still allows us to run our tests concurrently, and without any locking problems.
- We don't have any problems with rollbacks in our application code.
- However, it's less performant because of the overhead of database cloning and it may exhaust out your disk capabilities.
- Just like the previous approach, it needs a per-test application instance.
-->

---

# Tuning the Database

For test speed

- Mount database storage in a tmpfs-backed container

```sh
podman run --image-volume tmpfs
docker run --tmpfs /var/lib/postgresql/data
```

- Disable durability features: in case of a rare database crash, just re-run the tests

```ini
fsync=off 
full_page_writes=off
autovacuum=off
```

<!--
- Independently of the approaches I presented, if you use a real database in your tests, I have two tips for you.
- If you don't have an NVME SSD, your disk speed may become a bottleneck.
- You can use a tmpfs backed container to prevent that. This will write databases into working memory instead of your hard drive.
- You can switch off some durability features that your tests don't need.
- These features are built to guarantee safe recovery after a database crash, but for our tests we don't need persistent data, if something goes wrong we can just re-run the test suite.
- The settings listed here are for PostgreSQL.
-->

---
layout: quote
---

# 2k tests in 30 seconds
(not including compile time)

<!--
- This concludes our topic of database access.
- Using the database cloning technique in a recent project, it took about 30 seconds to run 2000 tests on a modern desktop PC, not including the compile time of course.
- Watching and re-running a subset of the tests when working on a specific feature takes almost no time.
- This is the kind of sweet feedback loop you can get when you run your tests in parallel :)
- And we've seen how we can achieve this speed while still catching mission-critical bugs like SQL query errors.
-->

---
layout: section
background: /crabtesting2.jpg
---

# 2. Concise Tests
For understandability and maintainability

<!-- 
- With the database tidy and tucked away, we can look at some rust code and how to make it more concise.
- This should also make test code easier to understand and maintain.
 -->

---

# Example of a Test

Using the axum web framework

```rs
struct Dog {
    id: Uuid,
    name: String,
}

#[tokio::test]
fn create_dog() {
    // ...
}
```

<!-- 
- In a fictional dog registry written using the axum framework, this is how a test might start out.
- We have a dog struct with a primary key and a name.
- This test wants to ensure that we can create a struct like this through our REST API.
 -->

---

```rs {all|2|4|5-16|17-22|23-25|all}
    // Example of a standard test in an axum app
    let app = TestingApp::new();

    let dog_data = json!({"name": "Pluto"});
    let request = Request::builder()
        .method(Method::POST)
        .uri("/dogs")
        .header(header::CONTENT_TYPE, mime::APPLICATION_JSON)
        .body(serde_json::to_vec(dog_data))
        .unwrap();

    let response = app.router
        .oneshot(request)
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::OK);

    // Check that the response body matches the `Dog` struct
    let body = to_bytes(response.into_body()).await.unwrap();
    let dog: Dog = from_slice(body).unwrap();

    // Check that our input was used
    assert_eq!(dog.name, "Pluto");
```

<!--
- This is how the test might continue
- It's a lot so let's go through it step by step
- *click* first we create an instance of our backend for testing, maye with its own transaction or database
- *click* next, we prepare some data to describe our new dog.
- *click* next, we send a POST request to the dogs endpoint with the prepared data as a JSON body.
- We do this via axum's router which accepts requests directly.
- *click* we check that the request succeeded and parse the response into our `Dog` struct.
- *click* finally, we assert that the response contains the data we expect.
- *click* This code is pretty much taken from the official axum examples.
- If you think of the conventions a typical REST API uses, there's a lot here that doesn't need to be written out explicitly in each test.
- Let's see how this test could be written instead.
-->

---

```rs {all|6-8|10}
    // The previous test, but with a request builder
    let app = TestingApp::new();

    let dog_data = json!({"name": "Pluto"});

    let dog: Dog = app.dogs()
        .create(dog_data)
        .await;

    assert_eq!(dog.name, "Pluto");
```

<!--
- This snippet does the exact same thing as the one we saw before.
- It creates a new testing backend, sends our dog request and checks that the result matches our expectations.
- We do this by implementing a pattern that I call a "request builder" that knows the ins and outs of our application.
- *click* Here, we create a request builder for the dogs endpoint specifically by calling the `dogs` method on our testing app struct. I'll show you how this is implemented in a minute.
- We can use this request builder to do common test operations such as CREATE, READ, UPDATE and DELETE on our backend.
- The request builder encodes conventions of our API and provides useful defaults.
- For example, in tests it is actually okay to panic, so we can remove most of the error handling machinery and concentrate on the happy path in our tests.
- The request builder also deserializes the response JSON into a certain response type by default.
- *click* The `dog` variable has the type of our Dog struct, and we can conveniently use it's fields for assertions.
-->

---

# Creating a Request Builder

```rs {1-6|7-23}
struct TestRequest<Response> {
    router: axum::Router,
    base_url: String, // e.g. "/dogs"
    response_type: PhantomData<Response>,
}

struct TestingApp {
    router: Router,
}

impl TestingApp {
    // ...

    fn dogs(self) -> TestRequest<Dog> {
        TestRequest {
            router: self.router,
            base_url: "/dogs".to_string(),
            response_type: PhantomData,
        }
    }
}
```

<!-- 

- So how does this request builder work?
- This is a pattern, not a library, because it needs to be tailor-made for your application, your conventions and your API shape.
- I'll show you a simple implementation that can be extended to match diverse use-cases.
- At the end of the talk, I'll link to an example repository that demonstrates a more extensive request builder implementation.
- So, The request builder struct we see here contains everything it needs to know to dispatch requests to a given endpoint.
    - The axum router to dispatch our requests to, 
    - the base URL for this API endpoint, 
    - and a PhantomData field that allows us to make this struct generic without actually containing a value with the `Response` type.
- *click* To construct the request builder, we implement a method on our testing application that returns a request builder suitable to the endpoint we want to talk to.
- We can see the URL for the dogs endpoint being configured.
 -->

---

# Implementing a Method

```rs {0|1|2|3-13|15-18}
impl<Response: DeserializeOwned> TestRequest<Response> {
    async fn create<Input: Serialize>(mut self, input: Input) -> Response {
        let request = Request::builder()
            .method(Method::POST)
            .uri(self.base_url)
            .header(header::CONTENT_TYPE, mime::APPLICATION_JSON)
            .body(serde_json::to_vec(input))
            .unwrap();

        let response = app.router
            .oneshot(request)
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);

        let body = to_bytes(response.into_body()).await.unwrap();
        from_slice(body).unwrap()
    }
}
```

<!--
- Now, to the implementation of the builder itself.
- It has a generic parameter that tells us which type we expect the backend to return when we dispatch the request. This type needs to be deserializable.
- *click* The function used to send a POST request takes any data that can be serialized as JSON.
- We also see why we have the generic Response type: we use it to indicate what we want the method to return.
- *click* We then send the request using the snippet we saw earlier.
- We serialize the input and send it as a json POST request.
- Note that we use the URL from the request builder's configuration.
- *click* We assert that the response status matches what we expect and return the deserialized body.
-->

---

# Request Builder Examples

```rs {all|0}
    let dog = app.dogs()
        .read(dog_id)
        .await;
```

```rs {0|all|0}
    let error = app.dogs()
        .expect_error(StatusCode::BAD_REQUEST)
        .create(json!({"bad": "data"}))
        .await;

    // Check that error message has the correct format
    assert!(error.message.starts_with("Failed to deserialize"))
```

```rs {0|all}
    let dogs = app.dogs()
        .query("page", 3)
        .list()
        .await;

    // Check that pagination returns correct number of rows
    assert_eq!(dogs.len(), 20)
```

<!--
- This is the basic implementation of the request builder. Let's look at what we could do with a more extensive implementation.
- This request builder provides the common CREATE, READ, UPDATE, DELETE operations.
- For example, we could fetch a given dog by its ID.
- *click* *click* If we expect an error, we can just say so by calling the `expect_error` method.
- The request builder will then return an error type which you can use to verify the response.
- The request builder's defaults allow us to focus on the interesting parts of the test.
- It gives us the flexibility to override these defaults when we need to.
- *click* *click* You can extend the request builder to your needs, such as query parameters.
- This could be used to check that pagination returns the correct number of rows, like in this example.
- Look how small all this code is! I think it's really easy to quickly grasp what's happening here.
- However your API works, you can adapt the request builder to succinctly describe common test operations.
- It can help you make your tests faster to write and easier to understand.
-->

---
layout: section
---

# 3. Generic Tests

<img src="/crabhats.jpg" alt="" class="rounded-md w-74 mx-auto mt-10">

<!-- 
- But let's use the request builder to go one step further.
- We'll use it to make our tests even more effective by writing a test once and then applying it to many similar cases.
 -->

---
layout: full
---

# Using the `rstest` Crate 

<!-- - todo Example of simplifying setup code -->

<div class="grid grid-cols-2 gap-x-4">

<div>

```rs {all|10|all}
#[tokio::test]
async fn validate_dog() {
    let app = TestingApp::new();

    let dog_data = json!({
        "name": "Spotty"
    });

    let dog = app.dogs()
        .validate(true)
        .create(dog_data)
        .await;

    assert_eq!(dog.name, "Spotty");
}
```

```rs {0|all}
#[tokio::test]
async fn create_dog() {
    // ...
}
```

</div>

```rs {0|all|1-3|5-6|15}
#[rstest]
#[case(true)]
#[case(false)]
#[tokio::test]
async fn create_and_validate_dog(
    #[case] validate: bool
) {
    let app = TestingApp::new();

    let dog_data = json!({
        "name": "Spotty"
    });

    let dog = app.dogs()
        .validate(validate)
        .create(dog_data)
        .await;

    assert_eq!(dog.name, "Spotty");
}
```

</div>

<!-- 
- Let's say we have added a validation function to our API, which allows us to validate a request without persisting anything.
- *click* We've added a corresponding method to our request builder and have written this test to check that the validation works.
- *click* *click* Our old test for creating a dog is pretty similar to this one.
- *click* The rstest crate allows us to join both tests into one.
- *click* We specify a list of cases and values, and for each case, the corresponding value *click* gets passed as a parameter.
- *click* We can then use this parameter to vary the test's behavior.
 -->

---

# Automatically Test *All* API Endpoints

```rs {all|7}
#[rstest]
#[case(TestingApp::new().dogs())]
#[case(TestingApp::new().treats())]
#[case(TestingApp::new().people())]
#[tokio::test]
async fn is_versioned<Response>(#[case] endpoint: TestRequest<Response>) {
    assert!(endpoint.base_url.starts_with("/v1/"));
}
```

<div class="mt-8"></div>

### Additional Inspiration

- Check error codes and formatting
- Check authentication requirements
- Add fixtures to request builder and check happy-path requests
- Check pagination functionality
<!-- 
- We can now use rstest to apply a single test to multiple API endpoints. 
- We simply pass in different request builders for each case to run the same test on multiple endpoints.
- *click* Encoding conventions in the request builder should encourage a consistent API style, but with this we can go further and actually enforce some constraints, for example the API versioning URL scheme.
- There are many possibilities of tests you can make generic, error checking, authentication, simple workflows like creating and reading an entity, or pagination.
- These tests will ensure that your API presents a consistent interface and behavior to the outside world, making it more reliable and maintainable.
- You might say that this is overengineered, but I'd argue that it is also fun, and having fun writing tests is important to write reliable software. I hope I've whet your appetite a little.
 -->

---
layout: two-cols
---

# Summary

<div class="mt-8"></div>

## 1. Database Access

<span class="opacity-60">Isolate with transactions or database cloning</span>

## 2. Concise Tests

<span class="opacity-60">Encode your conventions in a test request builder</span>

## 3. Generic Tests

<span class="opacity-60">Cover many cases with one test</span>

::right::

<div v-click>

<div class="grid grid-cols-[2em,1fr] justify-items-start items-center gap-y-3 ml-4 border border-gray-500 rounded-lg p-8 bg-zinc-800 shadow-sm shadow-zinc-600">
  <ri-link-m /> 
  <a href="https://www.rafa.ee">Find the slides on rafa.ee</a>
  <ri-github-line /> 
  <a href="https://github.com/raffomania/rust-web-testing-example">Read example code on GitHub</a>
  <ri-twitter-line /> 
  <a href="https://twitter.com/rafaelepplee">Contact me on twitter @rafaelepplee</a>
</div>

<img src="/crabmoon.jpg" alt="" class="rounded-md w-64 mx-auto mt-6">

</div>

<!-- 
- As a conclusion, we've looked at ways to make your tests fast and ergonomic. 
- We've seen techniques to handle your database efficiently.
- We've looked at the request builder pattern for writing more concise tests, and we've used that pattern together with some macros to cover many use cases in a single generic test.
- *click* I'm Rafael, feel free to chat me up after this or contact me on twitter.
- You can find the slides on my website, rafa.ee.
 -->
