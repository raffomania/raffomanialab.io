# End-to-end-testing Your Rust Backend

A talk for EuroRust 2022.

To start the slide show locally:

- `npm install`
- `npm run dev`
- visit http://localhost:3030