# Geheimwaffe Godot

_Wie man schnell und einfach stabile Spiele schreibt_

<br/>
<hr/>

Rafael Epplée \
[www.rafa.ee](https://www.rafa.ee) /
<img src="img/twitter.svg" height="31px" style="margin: 0; vertical-align: middle;"/> [@rafaelepplee](https://twitter.com/rafaelepplee)

//--

- 10 jahre spiele entwickelt
- JS, Unity, Love2D, Rust, und Godot

---

Aktuelles Projekt: 2 Planets

<video data-autoplay loop src="img/2planets.mp4"></video>

[Link: 2 Planets auf itch.io](https://2planets.itch.io/2planets)

<!-- .element: class="small" -->

//--

- 2planets -> godot
- ich mag open source -> godot
- Godot hat super tolle features
- nach Engine erfahrung fragen

---

## Was besprechen wir heute?

- Konzepte und Werkzeuge, die Godot besonders machen
- Tips, die in der Dokumentation etwas "versteckt" sind
- Prinzipien für robusten, anpassbaren Code, erklärt anhand von Godot

//--

- Fokus auf häufig benötigte Dinge
- Prinzipien erklärt anhand von Beispielen in Godot
- warum anpassbar?: refactoring wichtig für gutes game design
- moderne spiele werden häufiger geupdated
- Spiele sind nicht write-once code, da häufig experimentiert wird

---

## Agenda

- Signale
- Coroutines
- Szenen
- Debugging & Troubleshooting
- Animationen
- Editor tooling

//--

- Signale sind die grundlage
- coroutines sind in den docs gut versteckt
- Prinzipien, mit denen Szenen nützlich werden
- Ein Haufen verschiedener Techniken zum debuggen
- Vielseitigkeit von Animationen
- Praktische Editor-werkzeuge die Zeit sparen

---

## Signale: Agenda

- Lose Kopplung
- Signale selber definieren
- Beispiel mit und ohne Signalen

//--

- Was sind Signale: Code benachrichtigen, wenn etwas passiert
- Godot stellt viele Signale bereit: Button gedrückt, Animation zu ende, Timer durchgelaufen
- Lose Kopplung: Prinzip in der Softwareentwicklung
- Signale für Lose Kopplung

---

### Lose Kopplung / Kapselung

Abhängigkeiten zwischen Nodes minimieren \
für einfacheres Refactoring und Testing

//--

- Nodes einfach austauschen
- Nodes intern ändern, ohne Kommunikation mit anderen Nodes kaputt zu machen
- Nodes isoliert testen, ohne dafür viele andere Nodes erstellen zu müssen

---

### Beispiel: ohne Signale

![](img/coupled_nodes.svg)

//--

- Abhängigkeit von Coin label auf player: coin label will wissen, wann etwas passiert
- Direkter Methodenaufruf
- Player kann nicht ohne Coin label ausgeführt werden
- Änderungen am Coin label betreffen auch den Player
- Abhängigkeit in beide Richtungen
- Abhängigkeit vom Player auf coin label können wir los werden

---

### Beispiel: mit Signalen

![](img/decoupled_nodes.svg)

//--

- Player "emitted" `coin_added`-signal, das label reagiert darauf
- flexibler: Nodes können ausgetauscht werden
- testen: Einzelne Teile können getestet werden, mehr dazu später
- nächste slide: `coin_added` signal definieren
- danach: über editor oder script verbinden

---

### Eigene Signale definieren

<pre><code class="fragment language-gdscript" data-trim>
# player.gd

signal add_coin

func _process(dt):
    if coin_found:
        emit_signal('add_coin')
</code></pre>

<pre><code class="fragment language-gdscript" data-trim>
# coin_label.gd

var coins = 0

func _player_coin_added():
    coins += 1
</code></pre>

[Link: Godot docs zu Signalen](https://docs.godotengine.org/en/stable/getting_started/step_by_step/signals.html)

<!-- .element: class="small" -->

//--

- kommentar benennt dateinamen
- fragen, wer schon Signale verwendet hat
- fragen, wer weiß wie man über editor verbindet

---

![](img/signal_inspector.png)

//--

- Player auswählen
- Oben rechts im editor

---

![](img/signal_connect.png)

<!-- .element: class="stretch" -->

//--

- jedes mal bei `coin_added` wird die methode aufgerufen
- Über den editor verbundene Signale bleiben, wenn nodes verschoben werden
- dafür sieht man im script nicht die verbindung
- fragen, wer weiß, wie man über scripts verbindet

---

```gdscript
# coin_label.gd

var coins = 0

func _player_coin_added():
    coins += 1

func _ready():
    $player.connect('coin_added', self, '_player_coin_added')
```

[Link: Docs zu `get_node` / `$`](https://docs.godotengine.org/en/stable/classes/class_node.html#class-node-method-get-node)

<!-- .element: class="small" -->

//--

- `connect` methode mit signalname, aufruf-objekt und methode des objekts
- Wie kommen wir an die player-node? -> dollar-zeichen, das schauen wir uns auf der nächsten folie genauer an

---

`get_node() / $`

![](img/hierarchy_1.png)

<!-- .element: class="fragment" -->

<pre><code class="fragment language-gdscript" data-trim>
# coin_label.gd

get_node('player').connect()
$player.connect()
</code></pre>

![](img/hierarchy_2.png)

<!-- .element: class="fragment" -->

<pre><code class="fragment language-gdscript" data-trim>
$'../player'.connect()
</code></pre>

//--

- Ähnlich zu Pfaden in einem Dateisystem

---

![](img/hierarchy_3.png)

<pre><code class="fragment language-gdscript" data-trim>
$'../../world/player'.connect()
</code></pre>

//--

- bei `get_node()` muss der path angepasst werden, wenn nodes verschoben werden
- über den editor verbundene signale werden automatisch mitgenommen
- Fragen zu `get_node`?
- Als nächstes ein weiteres Beispiel zu loser Kopplung

---

### Beispiel: eng gekoppelt

```gdscript
# start_button.gd

extends Button

func _pressed():
    $'../game_manager'.start_game()
```

//--

- Geht kaputt
  - wenn button oder game manager verschoben werden
  - wenn im game manager `start_game` umbenannt wird
  - wenn man an einer zweiten stelle auf den knopfdruck reagieren möchte

---

### Beispiel: lose gekoppelt

```gdscript
# start_button.gd

extends Button

signal start_game

func _pressed():
    emit_signal('start_game')
```

```gdscript
# game_manager.gd

func _ready():
    $button.connect('start_game', self, '_on_start_game')

func _on_start_game():
    # start the game
```

//--

- Wir können nun
  - game manager verschieben, ohne den button anzupassen
  - von anderen stellen auf `start_game` reagieren
  - `start_game` methode beliebig anpassen
- nicht im editor verbunden, dafür sieht man im script die verbindung

---

### Signale: Zusammenfassung

- Lose Kopplung für Refactoring, Robustheit, Testen
- Signale definieren und verbinden

//--

- Signale brauchen etwas mehr code, aber lohnen sich
- Besonders für Kommunikation mit parent-nodes zu empfehlen
- Für child-nodes sind manchmal direkte methodenaufrufe pragmatischer
- Fragen?

---

## Coroutines

Funktionen "pausieren" und "wieder starten"

[Link: Docs zu coroutines](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_basics.html#coroutines-with-yield)

<!-- .element: class="small" -->

//--

- fragen, wer coroutines kennt

---

### Coroutines

```gdscript [|7|2-3|8-10|4|12]
func my_func():
    print('Hello')
    yield()
    print('world')

func _ready():
    var y = my_func()
    # Function state saved in 'y'.
    print(' my dear ')
    y.resume()

# output: 'Hello my dear world'
```

//--

- fragment für fragment code erklären
- ohne yield: "Hello world my dear"
- zwei resumes, aber nur ein yield -> fehler
- zwei yields -> zwei resumes

---

### Coroutines und Signale

```gdscript [|1-5|11-14|7-8|15]
# start_button.gd

extends Button

signal start_game

func _pressed():
    emit_signal('start_game')


# game_manager.gd

func _ready():
    yield($button, 'start_game')
    start_game()
```

[Link: Docs zu Coroutines mit Signalen](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_basics.html#coroutines-signals)

<!-- .element: class="small" -->

//--

- fragments: Über Coroutines pausieren, bis ein Signal feuert
- yield bekommt objekt und signalname
- Sehr unnützes Beispiel, kein vorteil ggü. connect()

---

### Coroutines: Beispiel Animation

```gdscript
$AnimationPlayer.play('shoot')

current_animation = 'shoot'
$AnimationPlayer.connect('animation_finished', self,
        '_on_animation_finished')

func _on_animation_finished():
    if current_animation == 'shoot':
        $AnimationPlayer.play('reload')
        current_animation = 'reload'
```

//--

- Praktischeres Beispiel: Animation `reload` nach `shoot` abspielen

---

```gdscript
$AnimationPlayer.play('shoot')

yield($AnimationPlayer, 'animation_finished')

$AnimationPlayer.play('reload')
```

//--

- Keine `current_animation` checks mehr!
- Der trick: durch `yield()` wissen wir, in welchem Zustand das Signal gefeuert wird
- ohne yield würde nur die reload animation spielen

---

### Coroutines: Beispiel cooldown

```gdscript
var fireball_on_cooldown = false

func _on_fireball_keypress():
    if fireball_on_cooldown:
        return

    cast_fireball()
    fireball_on_cooldown = true
    var cooldown_timer = get_tree().create_timer(5.0)
    cooldown_timer.connect('timeout', self, '_cooldown_over')

func _cooldown_over():
    fireball_on_cooldown = false
```

[Link: Docs zu `create_timer()`](https://docs.godotengine.org/en/stable/classes/class_scenetree.html#class-scenetree-method-create-timer)

<!-- .element: class="small" -->

//--

- Noch ein Beispiel für coroutines

---

```gdscript
var fireball_on_cooldown = false

func _on_fireball_keypress():
    if fireball_on_cooldown:
        return

    cast_fireball()
    fireball_on_cooldown = true
    yield(get_tree().create_timer(5.0), 'timeout')
    fireball_on_cooldown = false
```

//--

- Mein Lieblingsbeispiel, das ich ständig verwende
- So viel einfacher, zu verstehen was passiert
- Verschiedene methoden sind schwer zu überblicken

---

### Fragerunde

---

## Szenen

- Dateien organisieren
- Szenen kapseln
- Szenen testen

//--

- Szenen kapseln, um sie zu testen

---

### Ordnerstruktur

![](img/scene_folders_a.png)

//--

- Intuitive organisation
- Anfangs kein Problem

---

🧐

![](img/scene_folders_b1.png)
![](img/scene_folders_b2.png)
![](img/scene_folders_b3.png)

//--

- Was gehört zueinander?
- Häufig inkonsistente Benennung

---

👌

![](img/scene_folders_c1.png)
![](img/scene_folders_c2.png)

//--

- Arbeits-Kontext: nicht verschiedene scripts gleichzeitig, sondern alle ressourcen einer szene
- Eher persönliche Vorliebe
- Nicht unbedingt für große Teams, in denen z.B. einige nur Skripten

---

### Kleine Szenen

![](img/small_scenes_1.png)

<!-- .element: class="stretch" -->

//--

- Wir wollen jedem enemy einen Hut aufsetzen
- child-node zu jedem enemy hinzufügen ist aufwändig

---

![](img/small_scenes_2.png)

<!-- .element: class="stretch" -->

//--

- Szenen sind ähnlich zu prefabs aus Unity
- Vorlagen für Gruppen von Nodes

---

![](img/small_scenes_3.png)

//--

- klick auf das szenen-icon öffnet die unterszene zum editieren
- änderungen an allen enemy nodes wirksam
- gegner können in scripts instanziiert werden
- auch gut für: nächste slide

---

### Szenen testen

Jede Szene kann für sich alleine ausgeführt und getestet werden!

![](img/play_edited_scene.png)

//--

- Knopf oben rechts im editor
- kleine szenen sind gut zum debuggen
- Zum Beispiel vorher: Spieler-bewegung testen ohne von Gegnern genervt zu werden

---

![](img/debug_menubar.png)

- Änderungen an Szenen werden sofort ins laufende Spiel übernommen
- Änderungen an Skripten ebenso

//--

---

### Szenen zum testen isolieren

Beispiel: animierte health bar

<!-- .element: class="fragment" -->

![](img/testing_scene.png)<!-- .element: class="fragment" -->

//--

- Manche Szenen brauchen interaktion von außen
- z.B. health bar ist langweilig, wenn sich health nicht ändert
- Genaue Szenarien mit player und enemies hinkriegen ist tricky
- Lösung: kleine Test-szene drum rum bauen

---

```gdscript
# debug_health_bar.gd

while true:
    $health_bar.health -= (randi() % 20)
    if $health_bar.health <= 0:
        $health_bar.health = 100
    yield(get_tree().create_timer(randf()), 'timeout')
```

<video loop controls src="img/health-bar-demo.mp4" class="fragment"></video>

//--

- Beispiel: animationen testen
- Regelmäßig health ändern, ohne dass der Spieler stirbt
- szene mit "play scene" (f6) ausführen
- Alternative: direkt im editor testen, ohne Szene zu starten

---

### Testen mit `tool`

```gdscript [|1|3|5]
# health_bar.gd

tool

export var health = 100
```

![](img/export_inspector.png)

<!-- .element: class="stretch" -->

[Link: Docs zu `tool`](https://docs.godotengine.org/en/stable/tutorials/misc/running_code_in_the_editor.html#what-is-tool),
[Link: Docs zu `export`](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_exports.html)

<!-- .element: class="small" -->

//--

- Anpassung *in der health bar selbst*
- tool: scripts im editor _und_ im spiel laufen lassen
- Vorsicht, _alles_ wird ausgeführt!
- z.B. health exportieren, um sie im editor anzupassen

---

### Testen mit `tool`

```gdscript [|4]
# health_bar.gd

func _ready():
	if Engine.editor_hint:
        debug_loop()

func debug_loop():
    while true:
        health -= (randi() % 20)
        if health <= 0:
            health = 100
        yield(get_tree().create_timer(randf()),
                'timeout')
```

//--

- Vorheriges Beispiel direkt im Editor

---

### Gekapselte Szenen

- Kopplung nach außen verhindert einfaches testen:

```gdscript
$'../../game_manager'.start_game()
```

- Signale helfen hier häufig
- `get_node()` nach "innen" ist gekapselt:

```gdscript
$gun.shoot()
$AnimationPlayer.play('shoot')
```


//--

- Kopplung nach innen: Wir können meistens davon ausgehen, dass Kind-Szenen da sind

---

### Szenen: Zusammenfassung

- Szenenbasierte Ordnerstruktur
- Kleine Szenen
- Testbare, gekapselte Szenen

---

### Fragerunde

---

### Debugging

> "The most effective debugging tool is still careful thought, coupled with judiciously placed print statements." — Brian W. Kernighan

//--

- Bitte nicht zu ernst nehmen
- hat recht und unrecht zugleich
- debugging = daten und control flow verstehen

---

### Debugging: Agenda

- `print` clever einsetzen
- Nodes zur Laufzeit untersuchen und manipulieren
- Schritt-für-schritt-debugging

---

`print()`

```gdscript
var result = $some_child.do_the_thing().result
print("What does result even contain? ", result)

def _something_happened():
    print("something did actually happen!")
```

//--

- Unsicher, was für eine Klasse in einer variable steckt? print
- Unsicher, welche Werte in einer variable stecken? print
- Unsicher, ob eine Methode überhaupt aufgerufen wird? print

---

`print()` zeigt _alles_ an

```gdscript
print(["hello", "print"])
print({ "dictionaries": "check" })
print(Vector2(1, 0))
print(player_position, health, ammunition)
```

//--

- print nimmt mehrere Argumente an
- print zeigt, welche Klasse übergebene Werte haben

---

### Format strings

```gd
print("%d bottles of beer on the wall" % 99)
print("All your %s are belong to us" % "base")
$Label.text = "❤ %f, ammunition: %d" % [health, ammunition]
```

[Link: Docs zu format strings](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_format_string.html)

<!-- .element: class="small" -->

//--

- f für floats, d für decimals, s für strings
- mehrere argumente als liste
- geht auch ohne `print`

---

- Daten, die sich jeden Frame ändern, überschwemmen den log output
- Lösung: `Label`-Node in die Szene hinzufügen und Daten direkt im Spiel anzeigen

```gd
$Label.text = "position: (%f, %f)" % [position.x, position.y]
```

---

### Remote nodes

![](img/remote_inspection.png)

- Daten in Nodes während des laufenden Spiels sehen
- Zusammen mit "Scene/Script sync" ein starkes Werkzeug zum experimentieren

//--

- Sogar für spiele, die auf einem handy laufen und über kabel angeschlossen sind
- Erinnerung an "Szenen testen" -> sync setting

---

### Step debugger

- Funktioniert wie in anderen Sprachen
- Erlaubt feinste Einsicht in den Programmablauf

![](img/debug_breakpoints.png)

---

### Debugging: Zusammenfassung

- Von grob nach fein: `print()`, in-game overlays, Remote-inspektion, Step debugger
- Einzelne Szenen starten, um bugs schneller zu reproduzieren

---

### Animationen

<video loop data-autoplay src="/img/jump.mp4" ></video>

[Link: Docs zum animieren](https://docs.godotengine.org/en/stable/tutorials/animation/introduction.html)

<!-- .element: class="small" -->

---

### Die Grundlagen

Animationen und Keyframes anlegen

//--

- Fragen: Wer hat Erfahrung?
- Editor UI kann verwirrend sein, daher kurze Einführung

---

<img src="img/animation_editor.png" style="margin-bottom: 0; max-width: 80%;" />

1. `AnimationPlayer`-Node erstellen und Animation Editor öffnen
1. Neue Animation erstellen
1. Länge der Animation einstellen

---

<img src="img/animate_inspector.png" style="margin-bottom: 0; max-width: 30%;" />

<br />

4. Node auswählen
5. Keyframes für property einfügen
6. Animation direkt im Editor abspielen

//--

- Klick auf Schlüssel legt keyframe zum in der Timeline ausgewählten Zeitpunkt an
- Mehrere Keyframes -> Animation
- eine folie zurück und keyframes zeigen

---

### Was können Animationen?

- Sounds abspielen
- Funktionen aufrufen
- Andere Animationen starten
- Beliebige Properties von Nodes interpolieren

//--

- Position, Scale, Rotation etc. sind auch nur properties
- (fast) Alles in Godot ist eine Node
- Anderes Beispiel für properties: healthbar health

---

### Beispiele

- Sich bewegende Plattformen
- Aufblinken, um z.B. Verlust von Leben zu visualisieren
- Geräusche _genau_ zum richtigen Zeitpunkt abspielen
- Mehrere Animationen hintereinander abspielen

---

### Vorteile

- Exaktes timing
- Erlaubt schnelles anpassen und ausprobieren
- Kann angepasst werden, ohne den Code anzufassen
- Macht den Code kürzer und besser verständlich

---

### Nachteile

- Kann unübersichtlich werden
- Animations-editor ist gewöhnungsbedürftig
- Schwierig, dynamisch anzupassen (z.B. variable Abspielgeschwindigkeit)

---

<!-- ### Loading resources

todo

- preload: compile time
  - Catch errors before starting the game
  - prevent stuttering when loading a new scene
- relative paths
- Use `ResourceLoader` for background loading

--- -->

### Editor tooling

![](img/autocompletion.png)

`Ctrl+Space`: Autocompletion anzeigen

//--

- Nützlich, um den genauen Namen von Methoden zu finden
- Zeigt namen und typen der Parameter

---

![](img/jump_to_docs.png)
![](img/editor_docs_view.png)

`Ctrl+Click`: Dokumentation anzeigen

---

### Integration mit anderen Editors

- [VS Code](https://marketplace.visualstudio.com/items?itemName=geequlim.godot-tools)
- [Emacs](https://github.com/godotengine/emacs-gdscript-mode)
- [Vim](https://github.com/habamax/vim-godot)
- [IntelliJ IDEA](https://github.com/exigow/intellij-gdscript)

---

### Static typing

- Autocompletion
- Anzeige von Docs
- Fängt Fehler schon vor starten des Spiels

<pre><code class="fragment language-gd" data-trim>
func move_towards(direction):
    self.position += direction. # ???
</code></pre>

<pre><code class="fragment language-gd" data-trim>
func move_towards(direction: Vector2):
    self.position += direction.angle_to(self.position)
</code></pre>

[Link: Docs zu static typing](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/static_typing.html)

<!-- .element: class="small" -->

//--

- Sind optional, aber eine sehr gute Gewohnheit

---

Typen für Parameter und Variablen

```gd [1-2|3-4|6-9]
# Built-in types
var health: int = 0
# Infer type annotation
var is_shooting := false

# Core classes
def start_shooting(animation_player: AnimationPlayer):
    animation_player.play("shoot")
    is_shooting = true
```

[Link: Eingebaute Typen](https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_basics.html#built-in-types)

<!-- .element: class="small" -->

---

Eigene Klassen über `class_name` als Typen verwenden

```gd
# enemy.gd

class_name Enemy

var health := 0
```

```gd
# player.gd

def whack_enemy(enemy: Enemy):
    enemy.health -= 1
```

[Link: Docs zu class_name](https://docs.godotengine.org/en/stable/getting_started/step_by_step/scripting_continued.html#doc-scripting-continued-class-name)

<!-- .element: class="small" -->

---

### Editor tooling: Zusammenfassung

Type annotations helfen:

- Schneller code schreiben
- Schneller code verstehen
- Bugs schneller finden

---

### Geheimwaffe Godot: Zusammenfassung

Was macht Godot besonders (gut)?

- Signale
- Coroutines
- Szenen
- Animationen
- Unterstützendes Tooling
  - Zum debuggen
  - Zum code schreiben und verstehen

... für schnelleres Entwickeln mit weniger Bugs!

---

### Godot-Community

[Reddit](https://www.reddit.com/r/godot/), [Discord](https://discord.gg/zH7NUgz), [Matrix](https://matrix.to/#/#godotengine:matrix.org), [Q&A Forum](https://godotengine.org/qa/)

<video data-autoplay loop src="img/logo-meme.mp4" width="60%"></video>

[Credit: Sgt-Thorz on reddit](https://www.reddit.com/r/godot/comments/mcb2n0/using_the_godot_logo_as_placeholder_makes_for/)

<!-- .element: class="small" -->

//--

- Kreative Memes mit dem Godot-Logo
- Super Hilfsbereit und nett

---

### Danke für's zuhören!
