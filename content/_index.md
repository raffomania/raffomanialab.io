+++
title = "Hi, I'm Rafael"
template = "index.html"
+++

<div class="flex-list bleed mb-2">
    <div style="width: 15rem; flex-grow: 1; max-width: 28rem;">
        <h1>Hi, I'm Rafael</h1>
        <p class="mb-2">
            I'm a software engineer and game developer from Hamburg. I work on open source software fostering access to open knowledge, digital resilience and individual expression.
            <br>
            <a href="#work-with-me">Hire me for remote contracting work ⇢</a>
        </p>
        <div class="flex-list">
            <a href="https://mstdn.io/@raffomania">
                <img src="icons/mastodon.svg" alt="mastodon" class="icon" title="Mastodon"/>
            </a>
            <a href="https://github.com/raffomania">
                <img src="icons/github.svg" alt="github" class="icon" title="Source code"/>
            </a>
            <a href="https://www.mixcloud.com/lennythellama/">
                <img src="icons/headphones.svg" alt="mixcloud" class="icon" title="DJ Sets"/>
            </a>
        </div>
    </div>
    <img src="portrait.jpg" style="max-height: calc(12 * 1.65rem);" class="m-0 mt-2">
</div>
<p style="text-align: center" class="mb-2">
    <a href="https://last.fm/user/raffomania">
        <small>Currently listening to</small>
        <img src="https://toru.kio.dev/api/v1/raffomania?border_width=0&theme=nord" alt="Last.fm Activity" class="m-0 mx-auto" style="object-fit: contain;"/>
    </a>
</p>

## About Me

<!-- Born 1994, I quickly developed an eager interest in creative endeavours. Drawing and animating, manipulating photos using Photoshop, building papercraft, making small movies with my friends, modding Warcraft III... -->
<!-- The urge to always make something more colorful and exciting quickly led me to making my first games after discovering computers in my teens. When I started studying computer science, I kept on visiting gamejams and creating games in my free time, but quickly realized it was easier to make money by developing web applications. -->

As a software engineer, I work hard to produce maintainable, robust applications that are both useful to and usable for users. 
I like to enable good teams by fostering communication and documentation, optimizing workflows to reduce overhead and keeping an eye on the codebase as a whole to prevent technical road stops in the future.

As an artist, I'm fascinated by everything creative, and have tried my hand at painting, making electronic music, all kinds of generative art, 2D and 3D animation and storytelling. All of these come together in my passion for game development.


<!-- TODO:
- hobbies
- raya
- open source contributor
-->

## Projects

🃏 [knakk](https://raffomania.itch.io/knakk) \
A relaxed flip-and-write game. \
<small>Godot, Figma</small>

🔭 [archive.observer](https://archive.observer) \
Ad-free, fast, no-JS viewer for reddit archives. \
<small>Rust</small>

📻 [Song Sonar](https://songsonar.rocks) \
A smart Spotify playlist with new releases of artists you follow. \
<small>Rust</small>

[See more projects](/projects)

## Skills

I like to learn, and continue to adopt new languages and frameworks all the time. I'm also used to quickly working into existing codebases.

I have about a decade of professional web development experience in all the areas it touches: backend, frontend, SQL/NoSQL, DevOps, and traditional server administration. My favorite languages for web development are Elm and Rust, but I've gathered lots of professional experience in PHP, JS, TypeScript, Ruby and Python as well.

For making games, I use Godot, Blender, Aseprite and Reaper. In university, I've done some [VR Projects](https://raffomania.itch.io/dead-science) in Unity. I prefer tools that are open source and run on Linux, but try to stay pragmatic and will boot up a Windows VM if needed.

While German is my mother tongue, I'm a fluent English speaker and capable of basic communication in Spanish.

## Work With Me {#work-with-me}

I work on open-source software that interacts with people in meaningful ways, like [offline-first tools](https://github.com/raffomania/zero-bee), [accessible viewers for archived data](https://archive.observer) and [games](https://raffomania.itch.io/knakk). I tackle problems from a holistic perspective and take ownership of features and processes across the whole stack of a project.

If that sounds like a good fit, email me at <a href="mailto:hello@rafa.ee">hello@rafa.ee</a> or find me on <a rel="me" href="https://mstdn.io/@raffomania">Mastodon</a>.

<!-- <h2>Graphical Works</h2> -->
<!-- TODO: Add gallery -->
