+++
title = "Linkblocks Progress Update: January 2025"
+++

[Linkblocks has been selected for a grant by NLnet!](https://nlnet.nl/project/linkblocks/)

The grant's goal is to implement federation, full-text search, and collaborative features. This is the first progress report with some details around my process, and true to linkblocks' knowledge-sharing spirit, it contains a treasure trove of bookmarks discovered along the way :)

## ActivityPub

I've been reading up on the ActivityPub protocol, looking at implementations in other projects, and implementing federation in linkblocks.

I want linkblocks to push the boundaries of the UX around bookmarking, but I'm also interested in the future of federated software. To start, I tried to understand advantages and problems of the current state of the fediverse. Christine Lemmer-Webber's [OcapPub](https://gitlab.com/spritely/ocappub/blob/master/README.org) and [Content-Addressable Storage on top of ActivityPub](https://gitlab.com/spritely/golem/blob/master/README.org) writeups were enlightening, and I'm excited to see where the [Spritely Project](https://spritelyproject.org/) leads. [ActivityPub as it has been understood](https://flak.tedunangst.com/post/ActivityPub-as-it-has-been-understood) is a well-written deep dive into the details of currently deployed implementations. [This Guide for new ActivityPub implementers](https://socialhub.activitypub.rocks/t/guide-for-new-activitypub-implementers/479) is a comprehensive entrypoint for further reading.

## Linkblocks Status

Linkblocks' current, work-in-progress implementation of ActivityPub can read users from other instances. I've extended the current test tooling to spin up multiple instances, allowing me to easily write [tests for federation functionality](https://github.com/raffomania/linkblocks/blob/22b41d161aff5a44d09974773e20983df889775d/src/tests/federation.rs). 

Since fediverse users expect to locate others through usernames, I've added a process for SSO users to choose a username on their first login (I've found some motivation in [this Mastodon feature request](https://github.com/mastodon/mastodon/issues/21296)).

[Lemmy's activitypub library](https://github.com/LemmyNet/activitypub-federation-rust) has been a great building block, with excellent documentation. The [Ibis Wiki project](https://github.com/Nutomic/ibis) is a great example for real-world usage - it's a lot younger than lemmy and the smaller codebase makes it easy to read.

## FOSDEM

I went to FOSDEM and talked to lots of people who are super enthusiastic about the social web - it was an incredible experience. 

Casey, the author of [another federated bookmarking service](https://github.com/ckolderup/postmarks), gave [a great talk](https://motd.co/2025/02/fosdem-2025-postmarks/) about their experiences building federated software. Their idea of the "flattened web" speaks deeply to me: [information on current social media is heavily constrained by the "feed" format](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/). 

Organizing and presenting information in a structured graph (a web of links), allows us to escape the dopamine-ridden urge to discover "what's new", to go on deep dives around topics of interest, to rediscover forgotten gems. **If Mastodon is watching the news, linkblocks is browsing a library.**

Casey told me about [the Website League](https://websiteleague.org/), a project exploring [new ways to build a federated community](https://writer.oliphant.social/oliphant/islands). The "Nation-State’ification of the Fediverse", as Christine Lemmer-Webber calls it, is a real problem. I want to experiment with new modalities for user interaction and moderation in linkblocks to prevent this. I'll probably outline my ideas once I get to the collaborative part of the NLnet grant :)

I mentioned the future of federated software earlier - I spoke to Sébastien of [ActivityPods](https://activitypods.org/), a project I'll be watching closely. Sébastien also told me about [NextGraph](https://nextgraph.org/), which excites me because it is a) written in Rust and b) a powerful combination of local-first, peer-to-peer, and social technologies.

## Next Up

I'll flesh out federation for users, and add federation for bookmarks. I'll also make sure the federation works with Mastodon, and will try to make it work with Lemmy. In February, I'll be on vacation for three weeks, so the next progress report will reach you at the end of March. Thanks for stopping by, see you then!