+++
title = "Unity: Wishes for 2021"
date = 2020-12-02
+++

I've recently finished building a [VR project](https://raffomania.itch.io/dead-science) in Unity. Since I've worked with Unity before and have gathered a little experience, I'm going to talk about that and highlight some topics I'd love to see addressed in the next few years.

{{ side_note() }}

## Don't touch that

Reading on how to implement a UI, I found this table in the unity docs explaining the situation at hand:

<img src="unity-ui-libs.png" />

Apparently, there are three different toolkits, used for different scenarios and development styles. 
Some features are only available in one of them, some things are easier to do in another, and one of them is generally the preferred choice because it will replace the other two someday.
Chances are you'll be using all three of them at some time, multiplying the cognitive overhead as you switch between paradigms, file formats and APIs.

A similar, but more frustrating situation is the state of VR support in Unity. There's a legacy system and a new "plugin-based" system. The legacy system is deprecated, but the new system doesn't support SteamVR &ndash; in my case forcing me to use the legacy system, the [only reference to which I found in the Unity 5.4 docs](https://docs.unity3d.com/540/Documentation/Manual/VRReference.html). 

This seems to be a pattern with Unity development: after discovering a component has problems, a new one is written (or [bought](https://docs.unity3d.com/Manual/com.unity.textmeshpro.html)). 
But instead of getting replaced, the old component stays alive like a half-supported, half-documented zombie providing features the new component doesn't get because the old one is still there.

For me, this created an incredible amount of uncertainty while developing: *Am I using the right tool for this? Is this bug caused by me or by this unmaintained legacy system? How do I know if I used this right when the docs where last updated in 2015?*

When even a tutorial for the new VR system [tells you to use a Prefab from a folder named `XR Legacy Input Helpers`](https://docs.unity3d.com/Manual/configuring-project-for-xr.html), you can't help but feel like you're building on top of a tower of tech debt that can topple without warning. This confusion is a hurdle for people coming to Unity, and the complexity of having multiple competing solutions also complicates the workflows of seasoned Unity developers, even if they might not notice it a lot.

I've seen the struggle with balancing new features and cleaning up legacy code in the teams I've worked with, and I sympathize with the Unity devs when thinking about the pressure that's on them to ship fancy new features. However, It feels like the legacy systems are [piling up](https://forum.unity.com/threads/are-you-still-using-legacy-animation-system.289367/), damaging developer productivity and adoption of Unity in the long run.

The obvious solution to this is to prioritize refactoring tasks on the roadmap &ndash; but I think there's also a lesson to learn regarding software architecture. 
This issue highlights some of [the dangers of rewriting code from scratch](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/), and it's useful to keep this situation in your head when deciding whether to deprecate a piece of software or to refactor it and add backwards-compatible features. 

And if you decide to do the rewrite, pay attention to:

## Documentation & Communication

To find out what's up with these legacy systems, you often have to dig through old blog posts in the Unity archive, just to understand the context the different solutions were written in, plans for their future and their capabilities. Guidance, context and overview regarding Unity's capabilities is something I'm sorely missing from the official documentation.

While the Unity docs are expansive in general, they often lack in details crucial to specific problems. Missing docs for legacy parts like the old VR system are one example, [basic rules regarding colliders](http://answers.unity.com/answers/188809/view.html) are another. 

These are [sometimes addressed by Unity employees in the forums](https://forum.unity.com/threads/xr-plugins-and-subsystems.693463/). Some are buried in in hours of videos on [unity learn](https://learn.unity.com). The community provides the rest of information on [answers.unity.com](https://answers.unity.com), Reddit, blog posts and YouTube videos. 
Trying to understand a specific behavior of Unity results in a journey through 30-minute videos, threads advising some form of power cycling and posts explaining basic concepts already covered in official materials, all to find a fact that could have been provided in a single sentence in the docs.

Not to say that these resources are worthless. The Unity community is awesome, friendly and helpful. I just think Unity is missing a big opportunity in not channeling all these resources into a more structured, accessible format: 
Allowing the community to submit PRs for the official docs. (Or encouraging their own devs to write docs instead of forum posts, for that matter.) Linking from the documentation to specific videos in unity learn. Creating an index of links to resources covering specific topics.

There are many specific questions and problems google can't answer that a few sentences in the docs would clear up.

## Conclusion

Unity is incredibly capable in many ways. Improving on the existing codebase and documentation would make it even better: more stable, beginner-friendly and faster to work with.