+++
title = "Activism, Lowtech, Transhumanism and Sprouting: Links from the Solarpunk Camp 2024"
+++

Last week, we organized the [Solarpunk Camp 2024](https://solarpunkcamp.codeberg.page/), a gathering of people looking to make utopias imaginable and tangible. The event was better than I ever imagined, with incredibly generous and warm-hearted people exchanging knowledge and dreams about community, technology, activism, food and more. Thanks to everyone who came and made it amazing! Also, huge thanks to the folks at [WandelGut](https://wandelgut.de/) who provided an incredible location.

There was a wealth of talks, discussions and workshops that I can't cover in full; I'll talk about a few topics that stayed on my mind. [Since I believe that sharing websites is the best way to learn stuff on the web](https://www.rafa.ee/articles/introducing-linkblocks-federated-bookmark-manager/), I want to share a few of the links people brought to the camp.

## Sprouting

Sprouting has many nutritional benefits, is easy, and allows you to take part in the food production process. It's lowtech: Simple, robust, ubiquitous equipment and knowledge, leveraged to improve our lives. What could be more solarpunk?

- [Sprouting - GrimGrains](https://grimgrains.com/site/sprouting.html)

Apart from this guide on sprouting, GrimGrains has recipes as well as in-depth guides on nutrition, cooking equipment, and fermentation. Brought to you by [Hundred Rabbits](https://100r.co/site/home.html) - if you haven't heard of them, their website is a treasure trove of fascinating solarpunk projects.

## Salvagepunk

What happens when our current resources are exhausted? When all the oil has been fracked and all the minerals mined? Related to [permacomputing](http://permacomputing.net/), Salvagepunk is about building technology with the devices and equipment we already have lying around or discarded. It seeks to grow resilience and local autonomy. Don Blair gave a new perspective on our future, talked about the premises of Salvagepunk, and brought ideas for repurposing old technology to build resilience and autonomy.

- [Bike Trailers](https://edgecollective.io/projects/biketrailer/)
- [Biochar Water Filters](https://edgecollective.io/projects/water_biochar/)

Here are two of Don's easy to follow guides for building affordable lowtech equipment. You can find more of them on his site, [Edge Collective](https://edgecollective.io/).

- [Deadly Optimism, Useful Pessimism](https://richardheinberg.com/museletter-353-deadly-optimism-useful-pessimism)

Talking about the apocalypse is scary. It's easy to dismiss the conversation and lean on optimism to keep the despair at bay. This guide tells us why being pessimistic might be better for us and frames pessimism in a way that provides hope.

- [An approach to computing and sustainability inspired from permaculture](https://www.youtube.com/watch?v=T3u7bGgVspM&list=PLcGKfGEEONaBNsY_bOj8IhbCPvj6OAdWo&index=34)

I've been following the permaculture space for quite a while, and I've always wondered what "permaculture technology" would actually look like. This, for example, is how it would look like.

## Humans, Machines, Animals

We had an excellent talk about anarcho-transhumanism that I can't say much about, since it's a deep topic and I'm just beginning to read and think about it. It poses some very interesting questions about the relation between humans, machines and nature. 
As I understand it, anarcho-transhumanism is fundamentally different from transhumanism.
I view transhumanism itself with great skepticism.

- [Answer to a question about "A Cyborg Manifesto" on Philosophy StackExchange](https://philosophy.stackexchange.com/a/99612)

While Donna Haraway's work is not transhumanist, it's adjacent to some of its topics, and the questions she poses helped me gain the perspective I needed to get into the space.

- [Haraway on Cyborg Anthropology, Human-Machine Relations, and Racism](https://cyberartsweb.org/cpace/cyborg/anthro.html)

> Haraway proposes what she terms a "cyborg anthropology" to study the relation between the machine and the human, and she adds that it should proceed by "provocatively" reconceiving "the border relations among specific humans, other organisms, and machines" (52). One [...] unexpected result of such a provocative approach is the recognition that attempts to establish binary oppositions between human and machine, people and technology, has disturbing parallels with racism [...]

- [On Artificiality](https://www.capurro.de/artif.htm)

More food for thought about what it means to be human, and whether we can separate nature from "artificial" things:

> In his lectures “Un know-how per l'etica” Francisco Varela shows that computational artificiality, if it is supposed to bring forth cognitive phenomena, must operate on the basis of a a self-organizing system whose dynamic is only possible if there is no fixed program or no stable self. Not the computation but the interaction of the organism with its environment is a necessary condition of this dynamic. But the sufficient condition is the openness or the 'hole' between the system and its environment. This openness permits the system create its own world out of its environment. 


## Impact-Oriented Activism

Folks from the [WeiterSo! collective](https://www.weiterso.org/) talked about an impact-oriented approach to activism. They presented methods for actually making a difference and not burning out in the process.

- [What is Theory of Change?](https://www.theoryofchange.org/what-is-theory-of-change/)
- [Theory of change in ten steps](https://www.thinknpc.org/resource-hub/ten-steps/)

The Theory of Change is a structured approach for planning concrete steps to achieve change - when you have ambitious goals but don't know how to get there, here's a playbook you can try.

- [SMART Goals](https://www.mindtools.com/a4wo118/smart-goals)

"Saving the planet from climate change" is a noble goal, but if you don't want to burn out while reaching for the stars, here's a framework for choosing goals that work well. Folks in this talk added the additional two criteria "Impact-Oriented" (actually making a change) and "Self-Image" (aligned with our values and ethics).


## More

If you want more solarpunk and are somewhere near Germany, check out the [solpunk](https://solpunk.ukrudt.net/) event in August!

Also, [join our matrix room](https://matrix.to/#/#solarpunk-society:matrix.org) to chat with solarpunk-minded people or if you want to come to the Solarpunk Camp next year.

## The Future

The future can be a bright dream if you let it. That bright dream can become a reality, if you build it. Daily news of the impending apocalypse only paralyze us; Solarpunk offers a way out.