---
title: "Deploying Godot 4 HTML exports with cross-origin isolation"
date: 2022-11-14
---

Godot 4 games exported to HTML use a JavaScript feature called [SharedArrayBuffer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer).
This allows sharing memory between the main thread and service workers efficiently.
However, it makes it possible for scripts loaded from other pages to access this shared memory as well, which opens a page up to security vulnerabilities.

{{ side_note() }}

To prevent vulnerabilities, sites using the `SharedArrayBuffer` feature need to fulfill some security requirements:

- They must be in a [Secure Context](https://developer.mozilla.org/en-US/docs/Web/Security/Secure_Contexts), which is mostly achieved by serving the site over HTTPS.
- They need to set a suitable [Cross-Origin-Opener-Policy (COOP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Opener-Policy) and [Cross-Origin-Embedder-Policy (COEP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Embedder-Policy) header resulting in "Cross-Origin Isolation".
The standard values for these are `same-origin` and `require-corp` respectively, but [Chrome supports `credentialless`](https://developer.chrome.com/blog/coep-credentialless-origin-trial/) as an embedder policy as well.

Let's look at how we can fulfill these requirements when deploying our games to itch.io and Netlify.

## itch.io

[itch.io has experimental support for SharedArrayBuffers](https://itch.io/t/2025776/experimental-sharedarraybuffer-support).
All we have to do is tick the "SharedArrayBuffer support" checkbox in the `Embed Options > Frame Options` section of our game's settings page.

Setting the standard Cross-Origin Isolation headers breaks some features that interact with other domains. 
itch.io uses Chrome's `credentialless` option for the COEP header which lifts some of the restrictions imposed by the `require-corp` option.
Firefox does not implement `credentialless` at the time of writing, meaning Godot 4 HTML exports deployed on itch.io will only run in Chrome-based browsers.

## Netlify

In contrast to itch.io and other simple hosting options like GitHub Pages, Netlify [allows setting custom headers](https://docs.netlify.com/routing/headers/) using a `_headers` file or the `netlify.toml` configuration file.

Here's the entry in `netlify.toml` for setting the required cross origin policy headers to run both in Chrome and Firefox:

```toml
[[headers]]
for = "/*"

[headers.values]
Cross-Origin-Opener-Policy = "same-origin"
Cross-Origin-Embedder-Policy = "require-corp"
```

## Closing thoughts

It has only been about half a year since Chrome shipped `credentialless` support, and [Firefox is testing it using an Origin Trial](https://github.com/mozilla/standards-positions/issues/539#issuecomment-1224320026). 
Going forward, it's likely we'll see `credentialless` as the standard way to enable SharedArrayBuffers, resolving the itch.io Firefox incompatibility.

I'd love to deploy my games to GitHub Pages as well.
GitHub pages does not allow setting custom headers, but [there is a feature request for COOP/COEP with a potential workaround](https://github.com/community/community/discussions/13309) that uses a service worker to set the required headers.

## See also

- [Cross-origin isolation Guide on web.dev](https://web.dev/coop-coep/)
- [Chrome's post on credentialless support has a good explanation of Cross-Origin Isolation](https://developer.chrome.com/blog/coep-credentialless-origin-trial/)
- [Proxying GitHub Pages through Cloudflare and Heroku allows setting custom headers](https://www.rzegocki.pl/blog/custom-http-headers-with-github-pages/)
