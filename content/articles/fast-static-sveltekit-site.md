+++
title = "Optimizing a static SvelteKit site"
date = 2022-05-05
draft = true
+++

## Caching

- SvelteKit uses client-side navigation, but still explicitly loads data from endpoints on page changes
- Use `Cache-Control` via hook
    - `public, max-age=3600, stale-while-revalidate=86400`

## Prefetching

```js
import { browser } from '$app/env';
import { prefetchRoutes } from '$app/navigation';

if (browser) {
    prefetchRoutes();
}
```

- `prefetchRoutes()` prefetches all pages
- Alternatively, use `svelte:prefetch` attribute
- prefetch index endpoint explicitly

## Inlining styles

- `inlineStyleThreshold: 4096`
- `import style from $lib/css/inline.css;`
- works well for a separate "landing page" css file
- don't know if this works for normal svelte styles

## See also

- [https://github.com/vinayakkulkarni/s-offline]