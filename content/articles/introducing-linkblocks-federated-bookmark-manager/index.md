+++
title = "Introducing linkblocks, the Federated Bookmark Manager"
date = 2024-03-19
+++

<small>tl;dr: linkblocks is a tool for collecting and organizing bookmarks together. [Try out the linkblocks demo here.](https://linkblocks.zwielich.tech)</small>

The web is full of knowledge and art. Of course, that knowledge and art is hidden between websites full of soulless garbage. We need more ways to find the good stuff.

Search engines can work well, especially some of the [newer](https://kagi.com/) and [smaller ones](https://search.marginalia.nu/). They work really well when you want to search through a very wide set of topics, and when you are looking for something specific.

But there's another way of finding websites. It looks like a rabbit hole. You come across an entry point: an intriguing personal website, a fascinating Wikipedia article, a Reddit thread with some links. You start following links, opening tabs. You find surprising connections to other topics, or new views onto familiar terrain.

This kind of deep exploration works through *links* between websites. They are the threads you follow when searching your own way through the web graph. 
Links use connections between different pieces of information that are otherwise only present in our heads, and build a structure out of it, a structure that others can follow.

## A Companion for the Web

linkblocks is a tool for helping with this kind of deep exploration through the web.
With linkblocks, you can do three things: You can bookmark what you find on the web, you can structure your bookmarks, and you can exchange bookmarks with other people.
Organizing bookmarks works by linking them together, just like on the web. You can follow links to explore the websites you and others have collected. Bookmarking and linking on linkblocks is like publishing your own small website, saying "This is the good stuff".

<img src="linkblocks_screenshot.png" alt="A screenshot of the linkblocks UI, showing a page titled linkblocks. It contains a list of bookmarks, and a link to another list of bookmarks titled search engines." />

Publishing your own website is one way to share links, and [people already do this](https://hidde.blog/sharing-links/). But websites are a flexible medium and require some work and knowledge to set up. Even if you use hosted software like Wordpress, there's a blank page that you somehow have to shape.
linkblocks makes it simple by focusing on bookmarks only. There's no way to format text, upload images or build a custom layout like you would on your own website. Bookmarks are put into lists and linked together. It presents a well known and open way to arrange your library.
Focusing on collecting and sharing links also makes it easier for us to add features like website archival, a social activity feed or discussions.

## Now, and the Future

We've sketched out a basic UI for collecting and organizing bookmarks.
[You can try a demo here](https://linkblocks.zwielich.tech). It still needs polish, and the collaborative features are completely absent, but you can already get a feel for the "knowledge graph" way of structuring things.

For the next steps, we'll do user testing and refinement to make sure the bookmarking UI is intuitive and useful. We'll add more convenience features for taking notes, searching through bookmarks, and moving them around. Once the single-user bookmarking part feels solid, we'll refactor everything to add the social aspect - following other users, a discussion system, federation, moderation and privacy functionality, spaces with shared ownership.

If you'd like to follow linkblocks development, [follow me on mastodon](https://mstdn.io/@raffomania) for short updates, [subscribe to this site's feed](https://www.rafa.ee/atom.xml) for longer updates, or [watch the project on github](https://github.com/raffomania/linkblocks) for new releases.