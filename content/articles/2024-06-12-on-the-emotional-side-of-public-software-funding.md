+++
title = "On the Emotional Side of Public Software Funding"
+++

I just received the fourth rejection from a public funding program for one of my software projects. 

I'd like to tell you how this made me feel.
I have observed [others](https://ar.al/2024/06/01/small-technology-foundation-funding-application-for-nlnet-foundation-ngi-zero-core-seventh-call/) expressing [frustration](https://sleepmap.de/2023/operating-system-bias-in-next-generation-internet-and-nlnet/) and I think it’s healthy to discuss these emotions out in the open, and to find ways to deal with them together.

*This post is not a critique* of the way public funding programs are run right now.
People working there are doing their best within the system they have, and are doing invaluable work for the open source software community and our society as a whole.
I do not judge the way these programs are structured. I'll talk about how the application process made me feel, and how I'm dealing with that.

## It Hurts
The programs I applied for don’t give out much money when you compare them to a typical software development salary. But for a frugal person making the most of it, the funding can sustain more than a year of full-time work. For a small, new project like mine, that would have been a game changer. A year is enough to blow people’s minds with your project, to prove it works, to get attention and find more supporters. 
Losing this opportunity feels like the biggest setback possible, even though part of me knows it isn’t. It hurts. Why did I lose this opportunity?

## It’s My Fault
I had coaching sessions for writing applications.\
I took what worked from other successful applications. \
I selected organizations aligned with my project and vision. \
I got feedback on the text and rewrote it multiple times. \
I hit buzzwords that are hip right now. \
I applied to multiple programs and revised my applications for each one. \
I was sure I did everything right.\

And yet, I must have done something wrong. Is there something that others know about, but I don’t? I can't help but feel I'm not good enough, even though part of me knows I am. It's my fault. Why am I not good enough?

## I’m Angry
These programs say they aim to strengthen public software infrastructure. They want to improve open source software. They value data sovereignty, open access to information, privacy. My project is a perfect match! Do they not see that?

The feedback I received, if any, was short and vague. I know they get too many applications to provide more than that. But it makes me feel helpless. Am I supposed to try and try again without any chance to learn, to improve? 
I poured weeks of work and energy into my applications. I have gained nothing, learned nothing.
It feels like a punishment. 

When other people tell me it’s possible to increase your chance of success, it feels like they are deceiving themselves and everyone around them. The system feels rigged. It makes my insides burn with white hot rage.
I'm angry at strangers when I know they are doing their best, and my perspective is skewed. I'm helpless, confused, and angry. Why did they judge me like that? What did I do wrong? Why am I not good enough? Why did I lose this opportunity? Wh-

## Just Grow Up
Writing the applications already made me anxious. Getting rejected confirmed my insecurities and catapulted the anxiety to new heights. 
I feel other people would just tell me to grow up and deal with it. That’s just the way the world works, son. Sometimes you can’t have what you want. 

Maybe I’m just not made for this. I feel like some people might regard this whole post as childish, but writing it helps me deal with the situation.

## I’m Trying to Deal With It
The whole process has been a huge energy and time sink. I want to process my feelings and move on.

I feel discouraged to try to apply for public funding again, but I realize that this is just my frustration speaking. 
If I ever apply again, I want to try and find a way to distance myself from the process emotionally. 
I'll also spend less time on writing and preparation.
This is not because I believe it’s impossible to increase your chances of success, it’s just because I can’t find a way to do that.

I've started to collect other ways to fund a software project. Most of these have their own drawbacks, but it feels good to consider them and know that I'll eventually find a way:

- Work part time to cover living costs, work on my project the rest of the time
- Publish blog posts or other things regularly to attract an audience and start a Patreon/Liberapay/Github sponsors account
- Join an organization promoting universal basic income
- Create a company, look for investors, find a business model to support development
- Join an umbrella software organization that can provide advice and funding

Finally, it helps to contemplate that [I am going to die](https://www.todepond.com/wikiblogarden/sayings/i-am-going-to-die/). Nothing is as serious as it seems.