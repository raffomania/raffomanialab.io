---
title: Talks
---

## End-to-end Testing Your Rust Backend

<small>EuroRust 2022</small>

A talk showing approaches for improving test performance and developer experience around end-to-end tests for Rust web backend applications.

<a href="/talks/end-to-end-testing-your-rust-backend">Slides</a>
·
<a href="https://github.com/raffomania/rust-web-testing-example">Example project</a>

## Godot, Your Secret Weapon

<small>HAW Hamburg 2021, 2022</small>

A guest lecture for the "Human-Computer Interaction" course at HAW Hamburg. It shows intermediate and advanced features of the Godot engine that students use to create their own games.

## Overlapping Architecture: Implementation of Impossible Spaces in Virtual Reality Games

<small>Clash Of Realities Game Tech Summit 2021</small>

Presenting my Unity library for Impossible Spaces, a technique to enlarge a virtual space while allowing users to walk around normally.

<a href="/slides/2021-clash-of-realities-rafael-epplee-overlapping-architecture.pdf">Slides</a>
·
<a href="https://gitlab.com/raffomania/impossible-spaces">Implementation</a>