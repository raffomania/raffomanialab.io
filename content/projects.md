+++
title = "Projects"
+++

[linkblocks](https://github.com/raffomania/linkblocks) (2023&ndash;present)\
A federated website aggregator. \
<small>Rust</small>

🃏 [knakk](https://raffomania.itch.io/knakk) (2022&ndash;present)\
A relaxed flip-and-write game. \
<small>Godot, Figma</small>

🔭 [archive.observer](https://archive.observer) (2023&ndash;present)\
Ad-free, fast, no-JS viewer for reddit archives. \
<small>Rust</small>

📻 [Song Sonar](https://songsonar.rocks) (2018&ndash;present) \
A smart spotify playlist with new releases from artists you follow. \
<small>Rust</small>

🐝 [zero-bee](https://github.com/raffomania/zero-bee) (2020&ndash;2023) \
A zero-based budgeting app. \
<small>Elm</small>

🌑 [2 Planets](https://pomme-grenade.itch.io/2planets) (2019&ndash;2021) \
A fast-paced basebuilding duel in space. \
<small>Godot, Aseprite</small>

📌 [pinvault](https://github.com/raffomania/pinvault) (2020&ndash;2022) \
A personal, multi-media, distributed web archiving tool. Predecessor to [linkblocks](https://github.com/raffomania/linkblocks).\
<small>Rust</small>

🪐 [minin](https://github.com/raffomania/minin) (2022) \
A chill space mining game. \
<small>Elm</small>

🤖 [Aekisfour](https://github.com/raffomania/aekisfour) (2021) \
Logistics and base-building survival with autonomous spaceship robots. \
<small>Godot</small>

🃏 [Unnamed Game](https://github.com/Trixxo/3dpix) (2022) \
A tower-defense roguelike with cards. \
<small>Godot</small>

⏱ Unnamed Time Tracking Software (2022&ndash;present) \
A proof-of-concept for combining [automerge](https://automerge.org/) and [syncthing](https://github.com/syncthing/syncthing). \
<small>Rust</small>

📺 [mediaqbot](https://github.com/raffomania/mediaqbot) (2017&ndash;present) \
Telegram bot for building a mpv watch queue in telegram groups. \
<small>Rust, Python</small>

👓 [Dead Science](https://raffomania.itch.io/dead-science) (2020&ndash;2021) \
VR experiment combining impossible spaces and minimaps for my master's thesis. \
<small>Unity</small>

🏘 [Impossible Spaces for Unity](https://gitlab.com/raffomania/impossible-spaces) (2020&ndash;2021) \
Unity package implementing [Impossible Spaces](https://ieeexplore.ieee.org/abstract/document/6165136), a technique for enlarging virtual environments. \
<small>Unity</small>

🖼 [gradients](https://github.com/raffomania/gradients) (2018&ndash;2021) \
A wallpaper generator (try the "randomize" button). \
<small>ClojureScript</small>

📊 [3body](https://github.com/raffomania/3body) (2021) \
A particle-based music visualizer. \
<small>Godot</small>

🌱 [plants](https://github.com/raffomania/plants) (2018&ndash;2019) \
A little procedural animated plant generator. \
<small>Godot</small>

🍌 [lovetoys](https://github.com/lovetoys/lovetoys) (2014&ndash;2017) \
An entity component system library for the [love2d](https://love2d.org) game engine. \
<small>Lua</small>

🛑 [Hyper Oktagon](https://hyper-oktagon.github.io/) (2015) \
Glitchy survival minigame. \
<small>JavaScript</small>